<h1 align="center">Scrimba ReactFacts Static Project</h1>

<div align="center">
   Solution for a challenge from  <a href="http://devchallenges.io" target="_blank">Devchallenges.io</a>.
</div>

<div align="center">
  <h3>
    <a href="https://scrimbastaticproject1rajashekarmanda.netlify.app/">
      Demo
    </a>
    <span> | </span>
    <a href="https://gitlab.com/rajashekarmanda3729/reactstaticproject-1.git">
      Solution
    </a>
    <span> | </span>
    <a href="https://www.figma.com/file/y3vJGy5AixQ2AmQS3KPS7M/ReactFacts-(Copy)?node-id=0-4&t=h1wiA916HSQXKaJu-0">
      Challenge
    </a>
  </h3>
</div>

### UI Design 
![](src/images/deom.png)


### Setup
* if you want to start project with vite@reactJS use below command
*         npm create vite@latest
*         npm install
*         npm run dev
*         npm run build

the above commans for server run/install depedencies and build code.

### ReactJS Functionalities
* Used react-components & ReactDOM 
* Used CSS Flexbox for styling
* Used third party packages

### Features
  * Static design with some reactjs content.
  * With Header component and content component.

### Contact

* Github [@rajashekarmanda](https://github.com/Rajashekarmanda)