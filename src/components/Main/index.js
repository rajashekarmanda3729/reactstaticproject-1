import bkimage from '../../images/new.png'

import './style.css'

export default function Main() {
    return (
        <div className='content-container'>
            <h1 className="content-heading">Fun facts about React</h1>
            <div className='content'>
                <ul>
                    <li className="content-point">was released in 2013</li>
                    <li className="content-point">was orginally created by jordan</li>
                    <li className="content-point">Has well over 100k starts on Github</li>
                    <li className="content-point">Is maintained by facebook</li>
                    <li className="content-point">Powers thousands of enterpise apps, including mobile apps.</li>
                </ul>
                <img src={bkimage} alt="bk" className='bkimage' />
            </div>
        </div>
    )
}