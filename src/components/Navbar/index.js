import './style.css'
import reactlogo from '../../images/reactlogo.png'

function Header() {
    return (
        <div className="header">
            <nav className="logo-container">
                <img src={reactlogo} alt="logo" className="logo" />
                <h1 className="heading">ReactFacts</h1>
            </nav>
            <div>
                <p className="description">React course-Project 1</p>
            </div>
        </div>
    )
}
export default Header